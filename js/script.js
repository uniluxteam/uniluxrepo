/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
	$('.nav_trigger').click(function() {
        $('.main_nav').slideToggle();
    });
    $('.banner').bxSlider({
        auto: true,
        pager: false,
        nextSelector: '#banner_next',
        prevSelector: '#banner_prev',
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        }
    });
    $('.testimonial_slider').bxSlider({
        auto: true,
        speed: 1000,
        easing: 'ease-out'
    });
});
  WebFontConfig = {
    google: { families: [ 'Oxygen:400,700,300:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();

  WebFontConfig = {
    google: { families: [ 'Oxygen::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();

/*$(function () {
    var slider = $('.banner').bxSlider({
        pager: false,
        auto: true,
        prevSelector: '#mob_prev',
        nextSelector: '#mob_next',
        prevText: '<i class="ion ion-android-arrow-dropleft"></i>',
        nextText: '<i class="ion ion-android-arrow-dropright"></i>',
        onSlideAfter: function() {
            slider.stopAuto();
            slider.startAuto();
        }
    });*/